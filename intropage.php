<?php
session_start();

if(!isset($_SESSION["session_username"])):
	header("location: login.php");
else:
?>
	
<?php include("include/header.php"); ?>
<div id="welcome">
<h2>Wellcome, <span><?php echo $_SESSION['session_username'];?>! </span></h2>
  <p><a href="logout.php">Log out</a> of the system!</p>
</div>
<?php include("includes/footer.php"); ?>
<?php endif; ?>