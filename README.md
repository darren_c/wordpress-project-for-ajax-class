# README #

#Setup
-Requires the use of a database scheme. credentials must be placed in the include/constants.php file

-Database requires multiple tables such as usertbl.

-Database used for the project was not saved unfortunately. 

#Summary

This project was made for my AJAX class project. Focusing on AJAX, JavaScript, JQuery and PHP, we were tasked on creating a mock WordPress style site from scratch. 

#Credits
Credits to my teammates who worked on this project: Roman Kozlov, Maria Amigo, Joy Cao and Lucas Guariglia.

#Status
Project is currently closed as we satisfied our instructor's requirements.