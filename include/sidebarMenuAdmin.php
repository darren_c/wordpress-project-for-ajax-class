<?php session_start(); ?>
<?php 
$username = $_SESSION['session_username'];	
	
include('../include/connections.php');

$query = "select path from avatar where username = '$username';";
$result = mysql_query($query);
$check = mysql_fetch_row($result);
$_SESSION['avatar'] = $check[0];

$query1 = mysql_query("select * from avatar where username = '$username';");
if(mysql_num_rows($query1) == 0){
	$img = "images/img.jpg";
	$_SESSION['avatar'] = $img;
}
?>
<div class="navbar nav_title" style="border: 0;">
	<a href="index.php" class="site_title"><i class="fa fa-paw"></i> <span>Cool CMS</span></a>
</div>

<div class="clearfix"></div>

<div class="profile">
	<div class="profile_pic">
		<img src="<?php echo $_SESSION['avatar']; ?>" alt="..." class="img-circle profile_img">
	</div>
	<div class="profile_info">
		<span>Welcome,</span>
		<h2><?php echo $_SESSION['session_username']; ?></h2>
	</div>
</div>
<br/>
<h4 style="color: #2A3F54">General</h4>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<ul class="nav side-menu">
			<li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="index.php">Dashboard</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-image"></i> Media <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="allUploads.php">All Uploads</a></li>
					<li><a href="form_upload.php">Add New</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-clone"></i> Posts <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="allPosts.php">All Posts</a></li>
					<li><a href="addPost.php">Add New</a></li>
					<li><a href="editpost.php" style="display: none">Edit post</a></li>
				</ul>
			</li>
			<li><a><i class="fa fa-bar-chart-o"></i> Settings <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="settings.php">Change Avatar</a></li>
				</ul>
			</li>
			
			<li><a><i class="fa fa-user"></i> Users <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="addUser.php">Permissions</a></li>
				</ul>
			</li>
			
		</ul>
	</div>
</div>
<div class="sidebar-footer hidden-small">
	<a data-toggle="tooltip" data-placement="top" title="Settings">
		<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="FullScreen">
		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
	</a>
	<a data-toggle="tooltip" data-placement="top" title="Lock">
		<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	</a>
	<a href="../logout.php" data-toggle="tooltip" data-placement="top" title="Logout">
		<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	</a>
</div>