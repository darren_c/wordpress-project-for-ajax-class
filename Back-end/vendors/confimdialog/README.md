# jquery-bootstrap-confirm-modal
Easy to use Confirm Modal library properly created in Jquery and Bootstrap

# Requirements
- jquery.js (for functionality of button events)
- bootstrap.css (for rendering bootstrap modal UI)

# Methods
<pre>
.init(modalSize) - (Optional) Initialize the modal size. Values are 'sm' , 'md(by default)', 'lg'
.show(modalTitle, modalContent) - Show modal with customize title and content
.show(modalTitle, modalContent, customButtons) - Show modal with customize title and content and Button with Events
.hide() - Hide modal
</pre>

# Usage

Basic Modal Notification
<pre>
      Confirm.show('Modal Title', 'Modal Content');
</pre>

Save Confirmation 
<pre>
			Confirm.show('Modal Title', 'Modal Content', {
				'Save' : {
					'primary' : true,
					'callback' : function()
					{
						Confirm.show('Message', 'You have clicked Save');
					}
				}
			});
</pre>

Delete Confirmation
<pre>
			Confirm.show('Modal Title', 'Modal Content', {
				'Delete' : {
					'primary' : true,
					'callback' : function()
					{
						Confirm.show('Message', 'You have clicked Delete');
					}
				}
			});
</pre>

Three Buttons
<pre>
			Confirm.show('Modal Title', 'Modal Content', {
				'Save' : {
					'primary' : true,
					'callback' : function()
					{
						Confirm.show('Message', 'You have clicked Save');
					}
				},
				'Delete' : {
					'primary' : true,
					'callback' : function()
					{
						Confirm.show('Message', 'You have clicked Delete');
					}
				}
</pre>
