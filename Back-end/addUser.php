<?php
session_start();
if(!isset($_SESSION["session_username"]))
	header("location: ../login.php");
else{
?>

<?php include("../include/connections.php");
	$username = $_SESSION['session_username'];
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add New User</title>
	
    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="vendors/starrr/dist/starrr.css" rel="stylesheet">
		<!--Minimal Popup -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<script src="vendors/confimdialog/js/confirm.js"></script>
		
		<script src="vendors/confimdialog/js/jquery-2.1.4.js"></script>
		
    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
		
		<script>
			function addusertodb(){
				var email = $("#emailID").val();
				var selectdept = $("#departmentID option:selected" ).text();
				
				$.post("../include/addemailtodb.php",{
					emailID: email,
					deptID: selectdept
				},function(data){
					if(data == 1){//success in adding
						
						var z = "<div class=\"success nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Success!</strong> Successfully added Email to Database</div>";
						$(".nav-md").append(z);//add div to body;
					}
					else if(data==0){//unsuccessful in adding
						
						var z = "<div class=\"alert nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Danger!</strong> Failed to add to Database</div>";
						$(".nav-md").append(z);//add div to body;
					}
					else{
						alert(data);
					}
					
				})
				
			}
			function confirmuser(){ //confirms adding to user
				Confirm.show('Add User', 'Email is not in database. Add User?', {
					'Add' : {
						'primary' : true,
						'callback' : function()
						{
							addusertodb();
							Confirm.hide();
						}
					}
				});
				
				}
			function userindb(){
				Confirm.show('Email in db', 'Email is in database. Update Department?', {
					'Add' : {
						'primary' : true,
						'callback' : function()
						{
							updatedept();
							Confirm.hide();
						}
					}
				});
				
			}
			function updatedept(){
				var email = $("#emailID").val();
				var selectdept = $("#departmentID option:selected" ).text();
				$.post("../include/updatedept.php",{
					emailID: email,
					deptID: selectdept
				},function(data){
					if(data == 1){//success in adding						
						var z = "<div class=\"success nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Success!</strong> Successfully updated Department!</div>";
						$(".nav-md").append(z);//add div to body;
					}
					else{//if(data==0){//unsuccessful in adding						
						var z = "<div class=\"alert nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Danger!</strong> Failed to add to Databse</div>";
						$(".nav-md").append(z);//add div to body;
					}
					
				})
				
			}
			function addemailcheck(){
				var email = $("#emailID").val();
				var selectdept = $("#departmentID option:selected" ).text();
				
				if(email){ //if email is inputted
				
					$.post("../include/checkemailindb.php", {
					departmentID: selectdept,
					emailID: email
				}, function(data){
								
				if(data==0){//email is in db
					userindb();
				}
				else if(data ==1){//email isnt in db
					confirmuser();
				}
			
					});//end of $.get
						
				}//end of if
				else{//if no email
					//print error
					var z = "<div class=\"warning nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Danger!</strong> Enter All Fields Necessary!</div>";
						$(".nav-md").append(z);//add div to body;
				}
				
			}
			
		</script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">

            <!-- sidebar menu -->
            <?php include("../include/sidebarCheck.php"); ?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include("../include/topMenu.php"); ?>
        <!-- /top navigation -->
        
        
         <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">                  
                  <div class="x_content">
                    <div class="col-md-6 col-xs-12">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Add User to Department</h2>
                          <div class="clearfix"></div>
                        </div>
                            <div class="x_content">
                          <br />

                          <form action=""  id="formID" class="form-horizontal form-label-left">

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">E-Mail</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="email" class="form-control" placeholder="example@email.com" id="emailID" name="emailID">
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Department</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control" id="departmentID" name="departmentID">
																	<option>Human Resources</option>
																	<option>Sales</option>
																	<option>Help Desk</option>
                          			</select>
                              </div>
                            </div>
                            </form>
                            <div class="form-group">
                              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Cancel</button>
                                <button class="btn btn-success"  onclick="addemailcheck();">Submit</button>
                              </div>
                            </div>                         			

                        </div>
                      </div>
                    </div>

                  </div>
									
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        
        
        <!-- footer content -->
          <div class="pull-right">
            Made by Ye Cao, Maria Amigo, Darren Concepcion, Lucas Lucas, Roman
          </div>
          <div class="clearfix"></div>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="js/moment/moment.min.js"></script>
    <script src="js/datepicker/daterangepicker.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="vendors/starrr/dist/starrr.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="js/custom.js"></script>

  </body>
</html>
<?php } ?>
