<?php
session_start();

if(!isset($_SESSION["session_username"]))
	header("location: ../login.php");
else{
?>
<?php 
	include("../include/connections.php"); 
	$username = $_SESSION['session_username'];
	$pick = "select path from images where username = '$username';";
	$checkUser = mysql_query($pick);
?>
	<html lang="en">

	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

		<style>
			#uploads img {
				border-radius: 20px;
				width: 160px;
				height: 160px;
				;
				margin: 20px;
				border: 6px solid white;
				-webkit-transition: border 0.2s;
				transition: border 0.2s;
			}
			
			#uploads img:hover {}
			
			#uploads img:active {
				border: 6px solid #FFD040;
			}
			
			#uploads img.active {
				border: 6px solid #FFD040;
			}
		</style>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin Page</title>
		<!-- Bootstrap -->
		<link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- Custom Theme Style -->
		<link href="css/custom.css" rel="stylesheet">
	</head>

	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col">
					<div class="left_col scroll-view">

						<!-- sidebar menu -->
						<?php include("../include/sidebarCheck.php"); ?>
						<!-- /sidebar menu -->
					</div>
				</div>

				<!-- top navigation -->
				<?php include("../include/topMenu.php"); ?>
				<!-- /top navigation -->

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">
						<div class="page-title">
							<div class="title_left">
								<h3>Your Uploads</h3>
							</div>

							<div class="title_right">
								<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search for...">
										<span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
									</div>
								</div>
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2>Dropzone multiple file uploader</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
												<ul class="dropdown-menu" role="menu">
													<li><a href="#">Settings 1</a>
													</li>
													<li><a href="#">Settings 2</a>
													</li>
												</ul>
											</li>
											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div id="uploads">
											<?php 
		 									if(mysql_num_rows($checkUser) != 0){
												$id = 1;
												while ($row = mysql_fetch_row($checkUser)) {
													foreach($row as $field) {
															echo "<img id=\"$id\" src=\"$field\">";
															$id++;
													}
												}
											}
										?>
										</div>
									</div>
									<div id="buttonDelete"></div>
									<br/><br/><br/><br/><br/><br/><br/>
									
									<script>
											var check = $("#uploads img#1").attr("src");
											if (check)
												$("#buttonDelete").append("<button class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModal\">Delete Selected Images</button>");
									</script>
									
									<script>
										var idVal;
										var source;
										var values = [];
										var sources = [];

										$('#uploads img').click(function() {
											idVal = $(this).attr('id');
											source = $(this).attr('src');
											$(this).toggleClass('active');
											values.push(idVal);
											sources.push(source);
										});

										function del() {
											for (var i = 0; i < sources.length; i++) {
												var id = sources[i];
												//$(id).remove();
												deleteFromTable(id);
											}
											setInterval(function(){ window.location.reload(); }, 700);
										}

										function deleteFromTable(x) {
											$.post("../include/deleteImage.php", {
													srcID: x
												},
												function(data) {
													if (data == 1) {
														var z = "<div class=\"success nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Success!</strong> Select images deleted</div>";
														$(".nav-md").append(z);
													} else if (data == 0) {

														var z = "<div class=\"alert nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Danger!</strong> Failed to delete images</div>";
														$(".nav-md").append(z);
													} else {
														alert(data);
													}
												})
										}
									</script>

									<div id="myModal" class="modal fade" role="dialog">
										<div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Delete Confirmation</h4>
												</div>
												<div class="modal-body">
													<p>Are you sure you want to delete these images.</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
													<button type="button" class="btn btn-success" onclick="del()" data-dismiss="modal">YES</button>
												</div>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
					<div class="pull-right">
						Made by Ye Cao, Maria Amigo, Darren Concepcion, Lucas Lucas, Roman
					</div>
					<div class="clearfix"></div>
				<!-- /footer content -->
			</div>
		</div>
		<!-- jQuery -->
		<script src="vendors/jquery/dist/jquery.min.js"></script>
		<!-- Bootstrap -->
		<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- FastClick -->
		<script src="vendors/fastclick/lib/fastclick.js"></script>
		<!-- NProgress -->
		<script src="vendors/nprogress/nprogress.js"></script>

		<!-- Custom Theme Scripts -->
		<script src="js/custom.js"></script>
	</body>

	</html>
	<?php } ?>