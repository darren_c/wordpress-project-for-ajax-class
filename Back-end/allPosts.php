<?php
session_start();

if(!isset($_SESSION["session_username"]))
	header("location: ../login.php");
else{
?>
<?php include("../include/connections.php");
		 
	$username = $_SESSION['session_username'];
	$pick = "select path from images where username = '$username';";
	$checkUser = mysql_query($pick);
	//$t = "<img src=\"$result\">";
?>
<html lang="en">

<head>
	
<style>
	.page-title{
		margin-bottom: 50px;
	}
	.image{
		border-radius: 20px;
		width: 150px;
		height: 150px;;
		position: relative;
		display: absolute;
		margin: 20px;
	}
	
</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>All Posts</title>

  <!-- Bootstrap -->
  <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Dropzone.js -->
  <link href="vendors/dropzone/dist/min/dropzone.min.css" rel="stylesheet">
  <!-- Confirm-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="vendors/confimdialog/js/confirm.js"></script>		
		<script src="vendors/confimdialog/js/jquery-2.1.4.js"></script>
  <!-- Custom Theme Style -->
  <link href="css/custom.css" rel="stylesheet">
	<script>
		String.prototype.trunc = String.prototype.trunc ||
      function(n){
          return (this.length > n) ? this.substr(0,n-1)+'&hellip;' : this;
      };
		function retrieve(){
			var start="";
		$.post("../retrieve3.php",function(data){
						//confirm here
			if(data==0){
				alert("query failed");
			}
			else{							
			var astr = data.split('<divn>');		//split by post	
			for(var x = 0; x<astr.length-1; x++)
			{
				var y = "";
				var str = astr[x].split('<timeh>');	//split the time
				var ids = str[1].split('<ids>');//split the ids0 and post+title1
				var cot = ids[1].split('<title>'); //split the content1 and title0
				var pid = ids[0].trim().split(' ');//split the contentid0 and userid1
				var conti = cot[1].replace(/(<([^>]+)>)/ig,"");		
				conti = conti.trunc(300);
				start += "<div class=\"w3-card-4 w3-margin w3-white\"><div class=\"w3-container w3-padding-8\"><h3><div id=\"posttitle\">" +cot[0]+"</div></h3><hr></div><div class=\"w3-container\"><div id=\"postContent\"<p>"+conti+"<hr><a href=\"editpost.php?postid=" + pid[0] + "\">Edit</a> <a onclick=\"Confirmations("+pid[0]+")\">Delete</a></p></div></div></div>";
			}//end of forloop
				$("#postcontainer").html(start);				
			}
		});//end of ajax
	}//end of function
	$(document).ready(retrieve());
		
		function Confirmations(x){
			Confirm.show('Delete Post', 'Post will be deleted. Confirm?', {
					'Delete' : {
						'primary' : true,
						'callback' : function()
						{
							deletepost(x);
							Confirm.hide();
						}
					}
				});//end confirm.show
		}//end function confirm
		function deletepost(x){
			$.post("../include/deletepost.php",{pid: x}, function(data){
				if(data == 1){//success in adding
						
						var z = "<div class=\"success nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Success!</strong> Successfully Deleted from Database</div>";
						$(".nav-md").append(z);//add div to body;
					retrieve();
					}
					else if(data==0){//unsuccessful in adding
						
						var z = "<div class=\"alert nav_menu\"><span class=\"closebtn\" onclick=\"this.parentElement.style.display='none';\">&times;</span><strong>Danger!</strong> Failed to Delete</div>";
						$(".nav-md").append(z);//add div to body;
					}
			});
		}
	
	</script>
	
</head>

<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <!-- sidebar menu -->
          <?php include("../include/sidebarCheck.php"); ?>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
      <?php include("../include/topMenu.php"); ?>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
          <div class="page-title">
            <div class="title_left">
              <h3>All Posts</h3>
            </div>									
        	</div>
					<div id="posts">

						<div id="postcontainer"></div>
          </div>
      </div>
    </div>
  </div>
	<footer>
  <!-- footer content -->
		<div class="pull-right">
			Made by Ye Cao, Maria Amigo, Darren Concepcion, Lucas Lucas, Roman
		</div>
		<div class="clearfix"></div>
	</footer>
	<!-- /footer content -->

  <!-- jQuery -->
  <script src="vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- FastClick -->
  <script src="vendors/fastclick/lib/fastclick.js"></script>
  <!-- NProgress -->
  <script src="vendors/nprogress/nprogress.js"></script>
  <!-- Dropzone.js -->
  <script src="vendors/dropzone/dist/min/dropzone.min.js"></script>

  <!-- Custom Theme Scripts -->
  <script src="js/custom.js"></script>
</body>

</html>
<?php } ?>